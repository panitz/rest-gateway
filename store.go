package restgateway

import "io"

// Store bietet Methoden zum Zwischenspeichern von (Meta-)Daten an.
type Store interface {
	// AddItem fügt die Daten dem Store hinzu.
	AddItem(metaData MetaData, r io.Reader) error
	// GetItem liefert den Inhalt eines Elementes des Stores. Ist das gesuchte Element
	// nicht im Store vorhanden, dann muss ErrorItemNotFound zurückgegeben werden.
	GetItem(uuid string) (io.ReadCloser, error)
	// GetItemMetaData liefert die Metadaten eines Elementes im Store. Ist das gesuchte Element
	// nicht im Store vorhanden, dann muss ErrorItemNotFound zurückgegeben werden.
	GetItemMetaData(uuid string) (MetaData, error)
	// RemoveItem entfernt Daten und Metadaten eines Elementes aus dem Store. Ist das gesuchte Element
	// nicht im Store vorhanden, dann muss ErrorItemNotFound zurückgegeben werden.
	RemoveItem(uuid string) error
	// ListAllItems liefert die Metadaten der Elemente, die sich im Store befinden. Muss eine leere
	// Liste zurückgeben, wenn keine Elemente im Store vorhanden sind.
	ListAllItems() ([]MetaData, error)
}
