package restgateway_test

import (
	"bitbucket.org/panitz/rest-gateway"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"
)

var (
	testFileDir = getPath("temp", "test-file-store")
)

func TestNewFileStore(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)
	require.NotNil(t, store)
	require.True(t, exists(testFileDir), "Das Verzeichnis wurde nicht erstellt")
	require.True(t, exists(path.Join(testFileDir, ".meta")), "Das MetaData-Verzeichnis wurde nicht erstellt")
}

func TestFileStore_Add(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)

	now := time.Now()
	meta := restgateway.NewMetaData("text/plain")
	require.NotEmpty(t, meta.Uuid)
	require.True(t, meta.Timestamp.After(now))
	require.Equal(t, "text/plain", meta.ContentType)

	file, err := os.Open("testdata/textfile.txt")
	err = store.AddItem(meta, file)
	_ = file.Close()
	require.NoError(t, err)

	srcBytes, err := ioutil.ReadFile("testdata/textfile.txt")
	require.NoError(t, err)
	storeBytes, err := ioutil.ReadFile(filepath.Join(testFileDir, fmt.Sprintf("%d-%s.data", meta.Timestamp.UnixNano(), meta.Uuid)))
	require.NoError(t, err)
	require.Equal(t, srcBytes, storeBytes)

	var metaFromStore restgateway.MetaData
	metaBytes, err := ioutil.ReadFile(filepath.Join(testFileDir, ".meta", fmt.Sprintf("%d-%s.meta.json", meta.Timestamp.UnixNano(), meta.Uuid)))
	require.NoError(t, err)
	err = json.Unmarshal(metaBytes, &metaFromStore)
	require.NoError(t, err)

	meta.Timestamp = meta.Timestamp.Round(0)
	require.Equal(t, meta, metaFromStore)
}

func TestFileStore_GetItem(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)

	s := "Hello World"
	srcReader := bytes.NewReader([]byte(s))
	metaData := restgateway.NewMetaData("test")
	err = store.AddItem(metaData, srcReader)
	require.NoError(t, err)

	contentReader, err := store.GetItem(metaData.Uuid)
	require.NoError(t, err)
	//noinspection GoUnhandledErrorResult
	defer contentReader.Close()
	content, err := ioutil.ReadAll(contentReader)
	require.NoError(t, err)
	require.Equal(t, s, string(content))
}

func TestFileStore_GetItemMetaData_NotFound(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)
	_, err = store.GetItemMetaData("notExistingId")
	require.EqualError(t, err, restgateway.ErrorItemNotFound.Error())
}

func TestFileStore_GetItemMetaData(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)

	srcReader := bytes.NewReader([]byte("Some File Content"))
	metaData := restgateway.NewMetaData("test")
	err = store.AddItem(metaData, srcReader)
	require.NoError(t, err)

	storedMetaData, err := store.GetItemMetaData(metaData.Uuid)
	require.NoError(t, err)
	metaData.Timestamp = metaData.Timestamp.Round(0)
	require.Equal(t, metaData, storedMetaData)
}

func TestFileStore_RemoveItem_NotFound(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)
	require.EqualError(t, store.RemoveItem("notExistingUuid"), restgateway.ErrorItemNotFound.Error())
}

func TestFileStore_RemoveItem(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)

	meta := restgateway.NewMetaData("text/plain")
	srcReader := bytes.NewReader([]byte("Some File Content"))
	err = store.AddItem(meta, srcReader)
	require.NoError(t, err)
	require.True(t, exists(filepath.Join(testFileDir, fmt.Sprintf("%d-%s.data", meta.Timestamp.UnixNano(), meta.Uuid))))
	require.True(t, exists(filepath.Join(testFileDir, ".meta", fmt.Sprintf("%d-%s.meta.json", meta.Timestamp.UnixNano(), meta.Uuid))))

	err = store.RemoveItem(meta.Uuid)
	require.NoError(t, err)
	require.False(t, exists(filepath.Join(testFileDir, fmt.Sprintf("%d-%s.data", meta.Timestamp.UnixNano(), meta.Uuid))))
	require.False(t, exists(filepath.Join(testFileDir, ".meta", fmt.Sprintf("%d-%s.meta.json", meta.Timestamp.UnixNano(), meta.Uuid))))
}

func TestFileStore_ListAllItems(t *testing.T) {
	_ = os.RemoveAll(testFileDir)
	defer func() { _ = os.RemoveAll(testFileDir) }()
	store, err := restgateway.NewFileStore(testFileDir)
	require.Nil(t, err)

	createFile := func(uuid, content string) restgateway.MetaData {
		meta := restgateway.NewMetaData("text/plain")
		meta.Uuid = uuid
		meta.Timestamp = meta.Timestamp.Round(0)
		srcReader := bytes.NewReader([]byte(content))
		err = store.AddItem(meta, srcReader)
		require.NoError(t, err)
		return meta
	}

	file1 := createFile("1111", "Content file 1")
	file2 := createFile("2222", "Content file 2")
	file3 := createFile("3333", "Content file 3")

	files, err := store.ListAllItems()
	require.NoError(t, err)
	require.Len(t, files, 3)
	require.Equal(t, file1, files[0])
	require.Equal(t, file2, files[1])
	require.Equal(t, file3, files[2])
}

func getPath(elem ...string) string {
	return filepath.Join(elem...)
}

func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}
