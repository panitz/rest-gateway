package restgateway

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

const (
	DefaultSuccessCodes = "200,201,204,205"
)

// NewInMemoryFifoStrategy erstellt eine neue FifoStrategy, die die Elemente
// im Haupspeicher speichert und DefaultSuccessCodes verwendet.
func NewInMemoryFifoStrategy() (*FifoStrategy, error) {
	return NewFifoStrategy(NewMemoryStore(), DefaultSuccessCodes)
}

// NewFifoStrategy erstellt eine neue FifoStrategy und verwendet den übergebenen
// Store sowie die successCodes.
func NewFifoStrategy(store Store, successCodes string) (*FifoStrategy, error) {
	strategy := FifoStrategy{store: store}
	var err error
	strategy.successCodes, err = parseSuccessCodes(successCodes)
	if err != nil {
		return nil, err
	}
	return &strategy, nil
}

// FifoStrategy ist eine TransmissionStrategy-Implementation, bei der immer
// versucht wird, das erste Element zu senden.
type FifoStrategy struct {
	store        Store
	successCodes map[int]bool
}

// AddItem fügt das übergebene Element dem Store hinzu.
func (s *FifoStrategy) AddItem(metaData MetaData, r io.Reader) error {
	return s.store.AddItem(metaData, r)
}

// NextItem liefert das nächste Element, das gesendet werden soll. Nil signalisiert,
// dass kein Element gesendet werden soll. Es wird immer das erste Element im Store
// verwendet.
func (s *FifoStrategy) NextItem() (*Item, error) {
	items, err := s.store.ListAllItems()
	if err != nil {
		return nil, err
	}
	if len(items) == 0 {
		return nil, nil
	}
	meta := items[0]
	reader, err := s.store.GetItem(meta.Uuid)
	return &Item{MetaData: meta, reader: reader}, nil
}

// OnTransmissionFinished wird durch das Gateway nach dem Senden eines Elements
// aufgerufen. Die Strategy kann hierdurch auf das Ergebnis eines Sendeversuchs
// reagieren. Der Rückgabewert signalisiert dem Gateway, ob mit dem Senden
// des nächsten Elementes fortgefahren werden soll.
func (s *FifoStrategy) OnTransmissionFinished(res *TransmissionResult) bool {
	if res.Response == nil {
		return false
	}
	continueProcessing, found := s.successCodes[res.Response.StatusCode]
	if !found {
		return false
	}
	err := s.store.RemoveItem(res.MetaData.Uuid)
	return err == nil && continueProcessing
}

func parseSuccessCodes(successCodes string) (map[int]bool, error) {
	if successCodes == "" {
		successCodes = DefaultSuccessCodes
	}
	statusCodes := make(map[int]bool)
	codes := strings.Split(successCodes, ",")
	for _, code := range codes {
		statusCode, err := strconv.Atoi(code)
		if err != nil {
			return nil, fmt.Errorf("Fehler beim Konvertieren des Status-Codes: %s", err.Error())
		}
		statusCodes[statusCode] = true
	}
	return statusCodes, nil
}
