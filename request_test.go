package restgateway_test

import (
	"bitbucket.org/panitz/rest-gateway"
	"errors"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestNoopRequestExtender(t *testing.T) {
	// Dieser Test stellt sicher, dass keine Operation mit dem Request durchgeführt wird.
	// Dieses ist leicht zu prüfen, indem nil als Request übergeben wird. Sämtliche
	// Operationen schlagen in diesem Fall fehl.
	extender := restgateway.NewNoopRequestExtender()
	err := extender.ExtendRequest(nil)
	require.NoError(t, err)
}

func TestTokenAuthExtender_ExtendRequest_Error(t *testing.T) {
	provider := TokenProviderMock{
		TokenFunc: func() (string, error) {
			return "", errors.New("Some Error")
		},
	}
	e := restgateway.NewTokenAuth(&provider)
	err := e.ExtendRequest(nil)
	require.EqualError(t, err, "Some Error")
}

func TestTokenAuthExtender_ExtendRequest(t *testing.T) {
	provider := TokenProviderMock{
		TokenFunc: func() (string, error) {
			return "abc", nil
		},
	}
	e := restgateway.NewTokenAuth(&provider)
	req, err := http.NewRequest("POST", "http://example.com", nil)
	require.NoError(t, err)

	err = e.ExtendRequest(req)
	require.NoError(t, err)
	require.Equal(t, "Bearer abc", req.Header.Get("Authorization"))
}

func TestBasicAuthExtender_ExtendRequest(t *testing.T) {
	e := restgateway.NewBasicAuthExtender("müller", "göheim")
	req, err := http.NewRequest("POST", "http://example.com", nil)
	require.NoError(t, err)

	err = e.ExtendRequest(req)
	require.NoError(t, err)
	require.Equal(t, "Basic bcO8bGxlcjpnw7ZoZWlt", req.Header.Get("Authorization"))
}

type TokenProviderMock struct {
	TokenFunc func() (string, error)
}

func (m *TokenProviderMock) Token() (string, error) {
	if m.TokenFunc == nil {
		return "", nil
	}
	return m.TokenFunc()
}
