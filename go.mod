module bitbucket.org/panitz/rest-gateway

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/h2non/gock v1.0.12
	github.com/kr/pretty v0.1.0 // indirect
	github.com/magefile/mage v1.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.2.2
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
