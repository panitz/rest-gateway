package restgateway

import (
	"bytes"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"testing"
)

func TestMemoryStore_AddItem(t *testing.T) {
	store := NewMemoryStore()
	meta := NewMetaData("Text")
	content := "Some sample content"
	reader := bytes.NewReader([]byte(content))

	err := store.AddItem(meta, reader)
	require.NoError(t, err)
	element := store.items.Front()
	require.NotNil(t, element)
	item := element.Value.(memoryItem)
	require.Equal(t, meta, item.metaData)
	require.Equal(t, []byte(content), item.bytes)
}

func TestMemoryStore_AddItemWithCapacity(t *testing.T) {
	store := NewMemoryStoreWithCapacity(2)

	m1 := NewMetaData("")
	require.NoError(t, store.AddItem(m1, bytes.NewReader(nil)))
	require.Equal(t, 1, store.items.Len())
	require.Equal(t, m1, store.items.Front().Value.(memoryItem).metaData)

	m2 := NewMetaData("")
	require.NoError(t, store.AddItem(m2, bytes.NewReader(nil)))
	require.Equal(t, 2, store.items.Len())
	require.Equal(t, m1, store.items.Front().Value.(memoryItem).metaData)
	require.Equal(t, m2, store.items.Front().Next().Value.(memoryItem).metaData)

	m3 := NewMetaData("")
	require.NoError(t, store.AddItem(m3, bytes.NewReader(nil)))
	require.Equal(t, 2, store.items.Len())
	require.Equal(t, m2, store.items.Front().Value.(memoryItem).metaData)
	require.Equal(t, m3, store.items.Front().Next().Value.(memoryItem).metaData)
}

func TestMemoryStore_GetItem(t *testing.T) {
	store := NewMemoryStore()
	meta := NewMetaData("Text")
	content := "Some sample content"
	reader := bytes.NewReader([]byte(content))
	err := store.AddItem(meta, reader)
	require.NoError(t, err)

	itemContentReader, err := store.GetItem(meta.Uuid)
	require.NoError(t, err)
	itemContentBytes, err := ioutil.ReadAll(itemContentReader)
	require.NoError(t, err)
	require.Equal(t, content, string(itemContentBytes))
}

func TestMemoryStore_GetItemMetaData(t *testing.T) {
	meta := NewMetaData("Word Document")
	content := "Some test content"
	reader := bytes.NewReader([]byte(content))
	store := NewMemoryStore()
	err := store.AddItem(meta, reader)
	require.NoError(t, err)

	itemMetaData, err := store.GetItemMetaData(meta.Uuid)
	require.NoError(t, err)
	require.Equal(t, meta, itemMetaData)
}

func TestMemoryStore_ListAllItems(t *testing.T) {
	store := NewMemoryStore()
	m1 := NewMetaData("MD1")
	require.NoError(t, store.AddItem(m1, bytes.NewReader([]byte(("Content MD1")))))
	m2 := NewMetaData("MD2")
	require.NoError(t, store.AddItem(m2, bytes.NewReader([]byte(("Content MD2")))))
	m3 := NewMetaData("MD3")
	require.NoError(t, store.AddItem(m3, bytes.NewReader([]byte(("Content MD3")))))

	items, err := store.ListAllItems()
	require.NoError(t, err)
	require.Len(t, items, 3)
	require.Equal(t, m1, items[0])
	require.Equal(t, m2, items[1])
	require.Equal(t, m3, items[2])
}

func TestMemoryStore_RemoveItem(t *testing.T) {
	store := NewMemoryStore()
	meta := NewMetaData("MD1")
	require.NoError(t, store.AddItem(meta, bytes.NewReader([]byte(("Content MD1")))))

	require.Equal(t, 1, store.items.Len())
	err := store.RemoveItem(meta.Uuid)
	require.NoError(t, err)
	require.Equal(t, 0, store.items.Len())
}
