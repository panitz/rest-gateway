# rest-gateway

Implementation eines REST-Gateways für die Microservices.

- Übertragung beliebiger Content-Types an REST-Endpunkt
- Zwischenspeichern und erneute Übertragung bei Fehlern
    - Speicherung in Stores (Dateisystem, Memory, Datenbank, ...)
    - Strategien zum (erneuten) Senden, z.B. FIFO, LIFO, ...
- Authentication: JWT, Basic, ...
