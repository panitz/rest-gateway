package restgateway

import "context"

// NewNoopLogger erstellt einen neuen NoopLogger.
func NewNoopLogger() *NoopLogger {
	return &NoopLogger{}
}

// NoopLogger ist eine Logger-Implementation. Alle Methoden sind leer.
type NoopLogger struct{}

func (*NoopLogger) Error(_ Fields, _ ...interface{}) {}

func (*NoopLogger) ErrorCtx(_ context.Context, _ Fields, _ ...interface{}) {}

func (*NoopLogger) Warn(_ Fields, _ ...interface{}) {}

func (*NoopLogger) WarnCtx(_ context.Context, _ Fields, _ ...interface{}) {}

func (*NoopLogger) Info(_ Fields, _ ...interface{}) {}

func (*NoopLogger) InfoCtx(_ context.Context, _ Fields, _ ...interface{}) {}

func (*NoopLogger) Debug(_ Fields, _ ...interface{}) {}

func (*NoopLogger) DebugCtx(_ context.Context, _ Fields, _ ...interface{}) {}

func (*NoopLogger) Trace(_ Fields, _ ...interface{}) {}

func (*NoopLogger) TraceCtx(_ context.Context, _ Fields, _ ...interface{}) {}
