package restgateway_test

import (
	"bitbucket.org/panitz/rest-gateway"
	"bytes"
	"errors"
	"github.com/stretchr/testify/require"
	"io"
	"net/http"
	"testing"
)

func TestFifoStrategy_AddItem(t *testing.T) {
	var addWasCalled bool
	meta := restgateway.MetaData{Uuid: "abc"}
	reader := bytes.NewReader([]byte("Hello"))
	store := StoreMock{
		AddItemFunc: func(metaData restgateway.MetaData, r io.Reader) error {
			require.Equal(t, meta, metaData)
			require.Equal(t, reader, r)
			addWasCalled = true
			return nil
		},
	}
	s, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	err = s.AddItem(meta, reader)
	require.NoError(t, err)
	require.True(t, addWasCalled, "store.AddItem() was not called")
}

func TestFifoStrategy_AddItem_Error(t *testing.T) {
	meta := restgateway.MetaData{Uuid: "abc"}
	reader := bytes.NewReader([]byte("Hello"))
	store := StoreMock{
		AddItemFunc: func(metaData restgateway.MetaData, r io.Reader) error {
			return errors.New("error from store")
		},
	}
	s, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	err = s.AddItem(meta, reader)
	require.EqualError(t, err, "error from store")
}

func TestFifoStrategy_NextItem_StoreError(t *testing.T) {
	store := StoreMock{
		ListAllItemsFunc: func() ([]restgateway.MetaData, error) {
			return nil, errors.New("store error")
		},
	}
	strategy, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	item, err := strategy.NextItem()
	require.EqualError(t, err, "store error")
	require.Nil(t, item)
}

func TestFifoStrategy_NextItem_NoItemsInStore(t *testing.T) {
	store := StoreMock{}
	strategy, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	item, err := strategy.NextItem()
	require.NoError(t, err)
	require.Nil(t, item)
}

func TestFifoStrategy_NextItem_WithItems(t *testing.T) {
	metaData := restgateway.NewMetaData("text")
	store := StoreMock{
		ListAllItemsFunc: func() ([]restgateway.MetaData, error) {
			return []restgateway.MetaData{
				metaData,
				restgateway.NewMetaData("json"),
				restgateway.NewMetaData("binary"),
			}, nil
		},
	}
	strategy, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	item, err := strategy.NextItem()
	require.NoError(t, err)
	require.Equal(t, metaData, item.MetaData)
}

func TestFifoStrategy_OnTransmissionFinished_Error(t *testing.T) {
	meta := restgateway.NewMetaData("text")
	result := restgateway.TransmissionResult{
		MetaData: meta,
		Error:    errors.New("some error"),
		Response: nil,
	}
	// nil als Store garantiert, dass keine Kommunikation mit Store stattfindet.
	strategy, err := restgateway.NewFifoStrategy(nil, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	continueProcessing := strategy.OnTransmissionFinished(&result)
	require.False(t, continueProcessing, "Return value of OnTransmissionFinished() is not correct")
}

func TestFifoStrategy_OnTransmissionFinished_ErrorCode(t *testing.T) {
	meta := restgateway.NewMetaData("text")
	result := restgateway.TransmissionResult{
		MetaData: meta,
		Error:    nil,
		Response: &http.Response{StatusCode: http.StatusForbidden},
	}
	// nil als Store garantiert, dass keine Kommunikation mit Store stattfindet.
	strategy, err := restgateway.NewFifoStrategy(nil, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)

	continueProcessing := strategy.OnTransmissionFinished(&result)
	require.False(t, continueProcessing, "Return value of OnTransmissionFinished() is not correct")
}

func TestFifoStrategy_OnTransmissionFinished_SuccessCode(t *testing.T) {
	var removeWasCalled bool
	meta := restgateway.NewMetaData("text")
	store := StoreMock{
		RemoveItemFunc: func(uuid string) error {
			require.Equal(t, meta.Uuid, uuid)
			removeWasCalled = true
			return nil
		},
	}
	strategy, err := restgateway.NewFifoStrategy(&store, restgateway.DefaultSuccessCodes)
	require.NoError(t, err)
	result := restgateway.TransmissionResult{
		MetaData: meta,
		Error:    nil,
		Response: &http.Response{StatusCode: http.StatusOK},
	}

	continueProcessing := strategy.OnTransmissionFinished(&result)
	require.True(t, continueProcessing, "Return value of OnTransmissionFinished() is not correct")
	require.True(t, removeWasCalled, "store.RemoveItem() was not called")
}

type StoreMock struct {
	AddItemFunc         func(metaData restgateway.MetaData, r io.Reader) error
	GetItemFunc         func(uuid string) (io.ReadCloser, error)
	GetItemMetaDataFunc func(uuid string) (restgateway.MetaData, error)
	RemoveItemFunc      func(uuid string) error
	ListAllItemsFunc    func() ([]restgateway.MetaData, error)
}

func (m *StoreMock) AddItem(metaData restgateway.MetaData, r io.Reader) error {
	if m.AddItemFunc == nil {
		return nil
	}
	return m.AddItemFunc(metaData, r)
}

func (m *StoreMock) GetItem(uuid string) (io.ReadCloser, error) {
	if m.GetItemFunc == nil {
		return nil, restgateway.ErrorItemNotFound
	}
	return m.GetItemFunc(uuid)
}

func (m *StoreMock) GetItemMetaData(uuid string) (restgateway.MetaData, error) {
	if m.GetItemMetaDataFunc == nil {
		return restgateway.MetaData{}, nil
	}
	return m.GetItemMetaData(uuid)
}

func (m *StoreMock) RemoveItem(uuid string) error {
	if m.RemoveItemFunc == nil {
		return nil
	}
	return m.RemoveItemFunc(uuid)
}

func (m *StoreMock) ListAllItems() ([]restgateway.MetaData, error) {
	if m.ListAllItemsFunc == nil {
		return []restgateway.MetaData{}, nil
	}
	return m.ListAllItemsFunc()
}
