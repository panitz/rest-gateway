package restgateway

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
)

const (
	metaSubDir = ".meta"
)

// NewFileStore erstellt einen neuen FileStore mit den übergebenen Optionen.
// Die Daten werden im Verzeichnis dir gespeichert. Dieses Verzeichnis wird
// bei Bedarf erstellt.
func NewFileStore(dir string, opts ...FileStoreOption) (*FileStore, error) {
	if dir == "" {
		return nil, fmt.Errorf("Es wurde kein Verzeichnis für den FileStore angegeben.")
	}
	if err := createDirectory(dir); err != nil {
		return nil, err
	}
	metaDir := path.Join(dir, metaSubDir)
	if err := createDirectory(metaDir); err != nil {
		return nil, err
	}
	s := FileStore{
		dir:     dir,
		metaDir: metaDir,
	}
	for _, o := range opts {
		o(&s)
	}
	return &s, nil
}

// FileStoreOption ist eine Funktion zum Konfigurieren eines FileStore.
type FileStoreOption func(store *FileStore)

// FileStore ist eine Store-Implementation, der das Dateisystem zum Speichern
// der Daten verwendet.
type FileStore struct {
	dir     string
	metaDir string
	mux     sync.Mutex
}

// AddItem fügt die Daten dem Store hinzu.
func (s *FileStore) AddItem(metaData MetaData, r io.Reader) error {
	s.mux.Lock()
	defer s.mux.Unlock()

	// Payload speichern
	dataPath := s.dataPathForMetaData(metaData)
	dataFile, err := os.Create(dataPath)
	if err != nil {
		return err
	}
	//noinspection GoUnhandledErrorResult
	defer dataFile.Close()
	_, err = io.Copy(dataFile, r)
	if err != nil {
		return err
	}

	// MetaDaten speichern
	b, err := json.Marshal(metaData)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(s.metaDataPathForMetaData(metaData), b, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

// GetItem liefert den Inhalt eines Elementes des Stores. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *FileStore) GetItem(uuid string) (io.ReadCloser, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	metaData, err := s.GetItemMetaData(uuid)
	if err != nil {
		return nil, err
	}
	file, err := os.Open(s.dataPathForMetaData(metaData))
	return file, err
}

// dataPathForMetaData liefert den Dateipfad der Datendatei für das MetaData-Objekt.
func (s *FileStore) dataPathForMetaData(metaData MetaData) string {
	return filepath.Join(s.dir, fmt.Sprintf("%d-%s.data", metaData.Timestamp.UnixNano(), metaData.Uuid))
}

// metaDataPathForMetaData liefert den Dateipfad der Metadatendatei für das MetaData-Objekt.
func (s *FileStore) metaDataPathForMetaData(metaData MetaData) string {
	return filepath.Join(s.dir, metaSubDir, fmt.Sprintf("%d-%s.meta.json", metaData.Timestamp.UnixNano(), metaData.Uuid))
}

// GetItemMetaData liefert die Metadaten eines Elementes im Store. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *FileStore) GetItemMetaData(uuid string) (MetaData, error) {
	files, err := ioutil.ReadDir(s.metaDir)
	if err != nil {
		return MetaData{}, err
	}
	for _, file := range files {
		if strings.HasSuffix(file.Name(), fmt.Sprintf("-%s.meta.json", uuid)) {
			var metaData MetaData
			contentBytes, err := ioutil.ReadFile(filepath.Join(s.metaDir, file.Name()))
			if err != nil {
				return MetaData{}, err
			}
			err = json.Unmarshal(contentBytes, &metaData)
			if err != nil {
				return MetaData{}, err
			}
			return metaData, nil
		}
	}
	return MetaData{}, ErrorItemNotFound
}

// RemoveItem entfernt Daten und Metadaten eines Elementes aus dem Store. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *FileStore) RemoveItem(uuid string) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	metaData, err := s.GetItemMetaData(uuid)
	if err != nil {
		return err
	}
	if err := os.Remove(s.metaDataPathForMetaData(metaData)); err != nil {
		return err
	}
	if err := os.Remove(s.dataPathForMetaData(metaData)); err != nil {
		return err
	}
	return nil
}

// ListAllItems liefert die Metadaten der Elemente, die sich im Store befinden. Es wird eine leere
// Liste zurückgeben, wenn keine Elemente im Store vorhanden sind.
func (s *FileStore) ListAllItems() ([]MetaData, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	var items []MetaData
	files, err := ioutil.ReadDir(s.metaDir)
	if err != nil {
		return items, err
	}
	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".meta.json") {
			var metaData MetaData
			contentBytes, err := ioutil.ReadFile(filepath.Join(s.metaDir, file.Name()))
			if err != nil {
				return items, err
			}
			err = json.Unmarshal(contentBytes, &metaData)
			if err != nil {
				return items, err
			}
			items = append(items, metaData)
		}
	}
	return items, nil
}

// createDirectory stellt sicher, das das übergebene Verzeichnis existiert.
func createDirectory(dir string) error {
	return os.MkdirAll(dir, os.ModePerm)
}
