package restgateway

import "context"

// Fields sind zusätzliche Daten, die in den Logausgaben erscheinen.
type Fields map[string]interface{}

// Logger bietet Methoden zum Loggen von Meldungen mit unterschiedlichen
// Log-Leveln.
type Logger interface {
	// Error protokolliert eine Meldung mit Log-Level Error.
	Error(fields Fields, args ...interface{})
	// ErrorCtx protokolliert eine Meldung mit Log-Level Error.
	ErrorCtx(ctx context.Context, fields Fields, args ...interface{})

	// Warn protokolliert eine Meldung mit Log-Level Warn.
	Warn(fields Fields, args ...interface{})
	// WarnCtx protokolliert eine Meldung mit Log-Level Warn.
	WarnCtx(ctx context.Context, fields Fields, args ...interface{})

	// Info protokolliert eine Meldung mit Log-Level Info.
	Info(fields Fields, args ...interface{})
	// InfoCtx protokolliert eine Meldung mit Log-Level Info.
	InfoCtx(ctx context.Context, fields Fields, args ...interface{})

	// Debug protokolliert eine Meldung mit Log-Level Debug.
	Debug(fields Fields, args ...interface{})
	// DebugCtx protokolliert eine Meldung mit Log-Level Debug.
	DebugCtx(ctx context.Context, fields Fields, args ...interface{})

	// Trace protokolliert eine Meldung mit Log-Level Trace.
	Trace(fields Fields, args ...interface{})
	// TraceCtx protokolliert eine Meldung mit Log-Level Trace.
	TraceCtx(ctx context.Context, fields Fields, args ...interface{})
}
