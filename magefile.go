// +build mage

package main

import (
	"fmt"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

const (
	tempDir = "temp"
)

// Entfernt alle erstellten Artefakte, temporäre Dateien, etc.
func Clean() {
	var err error

	items := []string{
		tempDir,
	}
	for _, item := range items {
		fmt.Printf("Lösche %s... ", item)
		err = sh.Rm(item)
		if err != nil {
			fmt.Printf("FEHLER\n%s konnte nicht gelöscht werden: %s\n", item, err)
		} else {
			fmt.Println("OK")
		}
	}
}

// Formatiert den Quellcode.
func Fmt() error {
	return sh.RunV("go", "fmt", "./...")
}

// Führt die Tests aus.
func Test() error {
	mg.Deps(Clean, Fmt)
	return sh.RunV("go", "test", "./...")
}

// Führt Aufgaben vor einem Commit aus.
func PreCommit() {
	mg.SerialDeps(Test)
}
