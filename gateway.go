package restgateway

import (
	"errors"
	"github.com/satori/go.uuid"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

const (
	DefaultBackOffDuration = time.Second * 5
)

var (
	ErrorEndpointFuncMissing = errors.New("Es wurde keine Funktion zur Ermittlung des Endpunkts angegeben.")
	ErrorItemNotFound        = errors.New("Das gesuchte Element ist nicht im Store gespeichert.")
	ErrorNoItemsInStore      = errors.New("Es befinden sich keine Elemente im Store.")
)

// Options konfigurieren ein Gateway.
type Options struct {
	// Ein statischer Endpunkt, an den die Daten gesendet werden sollen. Entweder
	// Endpoint oder EndpointFunc muss angegeben werden.
	Endpoint string
	// Funktion zum Bestimmen eines dynamischen Endpunkts. Entweder
	// Endpoint oder EndpointFunc muss angegeben werden.
	EndpointFunc EndpointFunc
	// Ein Client, der zum Senden der Daten verwendet werden soll. Hierdurch können
	// die Daten z.B. verschlüsselt übertragen werden. Es wird http.DefaultClient
	// verwendet, wenn kein Client angegeben wurde.
	Client Client
	// Gibt die zu verwendende Übertragungsstrategie an. Hierdurch kann das Sendeverhalten
	// des Gateways konfigueriert werden. Hierzu zählen z.B. das persistente
	// Zwischenspeichern der Daten und das Fehlerverhalten. Wenn keine Strategie
	// angegeben wurde, dann wird NewInMemoryFifoStrategy() verwendet.
	// Achtung: NewInMemoryFifoStrategy() speichert die Daten nur im RAM zwischen.
	// Daten gehen bei einem Neustart des Programms verloren!
	Strategy TransmissionStrategy
	// RequestExtender bieten eine Methode zum Modifizieren des Requests vor dem
	// Senden. Hierdurch ist es z.B. möglich, den Request um Authentifizierungsinformationen
	// zu erweitern.
	RequestExtender RequestExtender
	// Zeitspanne, die nach fehlerhaften Übertragungen gewartet werden soll.
	BackOffDuration time.Duration
	// Logger wird zum Loggen von Meldungen verwendet.
	Logger Logger
}

// New erstellt ein neues RestGateway und gibt dieses zurück.
func New(opts Options) (*RestGateway, error) {
	err := applyDefaultsAndCheck(&opts)
	if err != nil {
		return nil, err
	}
	gw := RestGateway{
		endpointFunc:    opts.EndpointFunc,
		client:          opts.Client,
		strategy:        opts.Strategy,
		requestExtender: opts.RequestExtender,
		backOffDuration: opts.BackOffDuration,
		logger:          opts.Logger,
	}
	return &gw, nil
}

// NewMetaData erstellt ein neues MetaData-Objekt. Es wird eine neue UUID-V4 generiert
// und die aktuelle Zeit als Timestamp zugewiesen.
func NewMetaData(contentType string) MetaData {
	return MetaData{
		Uuid:        uuid.NewV4().String(),
		Timestamp:   time.Now(),
		ContentType: contentType,
	}
}

// MetaData speichert die Metadaten eines Store-Elementes.
type MetaData struct {
	// Eine eindeutige UUID für dieses Element.
	Uuid string `json:"uuid"`
	// Timestamp enthält den Zeitpunkt, an dem die Metadaten erstellt wurden.
	Timestamp time.Time `json:"timestamp"`
	// ContentType enthält den MIME-Type. Dieser wird als Content-Type-Header beim
	// Senden der Daten verwendet.
	ContentType string `json:"contentType"`
}

// Client wird zum Senden der Daten an den REST-Endpunkt verwendet.
type Client interface {
	// Do sendet den HTTP-Request.
	Do(req *http.Request) (*http.Response, error)
}

// RequestExtender bieten eine Methode zum Modifizieren des Requests vor dem
// Senden. Hierdurch ist es z.B. möglich, den Request um Authentifizierungsinformationen
// zu erweitern.
type RequestExtender interface {
	// ExtendRequest wird vor dem Senden für jeden Request aufgerufen. Der Request kann
	// so modifiziert werden.
	ExtendRequest(req *http.Request) error
}

// TransmissionResult enthält das Ergebnis einer Datenübertragung.
type TransmissionResult struct {
	// Die Metadaten des Requests.
	MetaData MetaData
	// Die Response, die von Client.Do(*http.Request) empfangen wurde. Kann nil
	// sein, wenn Fehler aufgetreten sind.
	Response *http.Response
	// Error enthält einen aufgetretenen Fehler. Ist nil, wenn erfolgreich.
	Error error
}

func (r *TransmissionResult) DrainAndCloseBody() {
	if r.Response == nil {
		return
	}
	body := r.Response.Body
	_, _ = io.Copy(ioutil.Discard, body)
	_ = body.Close()
}

// Item enthält die Daten eines zu übertragenen Elementes.
type Item struct {
	// Reader für den Zugriff auf die Daten des Elementes.
	reader io.ReadCloser
	// Metadaten des Elementes.
	MetaData MetaData
}

// Close schliesst den zugeordneten Reader.
func (i *Item) Close() error {
	if i.reader == nil {
		return nil
	}
	return i.reader.Close()
}

// Read kapselt die Read-Methode des Readers.
func (i *Item) Read(p []byte) (n int, err error) {
	if i.reader == nil {
		if len(p) == 0 {
			return 0, nil
		}
		return 0, io.EOF
	}
	return i.reader.Read(p)
}

// TransmissionStrategy stellt Methoden zur Verfügung, mit der die Arbeitsweise
// beim Versenden von Elementen durch das RestGateway beeinflusst werden kann.
// Denkbar wären Strategien wie LIFO, FIFO, etc.
type TransmissionStrategy interface {
	// AddItem Übergibt ein neues Element zum Senden an den REST-Endpunkt. Die Strategie
	// speichert in der Regel das Element samt Metadaten in einem Store hinzu.
	AddItem(metaData MetaData, r io.Reader) error
	// NextItem liefert das nächste Element, das gesendet werden soll. Nil signalisiert,
	// dass kein Element gesendet werden soll.
	NextItem() (*Item, error)
	// OnTransmissionFinished wird durch das Gateway nach dem Senden eines Elements
	// aufgerufen. Die Strategy kann hierdurch auf das Ergebnis eines Sendeversuchs
	// reagieren. Der Rückgabewert signalisiert dem Gateway, ob mit dem Senden
	// des nächsten Elementes fortgefahren werden soll.
	OnTransmissionFinished(*TransmissionResult) bool
}

// EndpointFunc ist eine Funktion, die den Endpunkt für ein MetaData-Objekt liefert.
// Hierdurch können Endpunkte dynamisch anhand von Metadaten generiert werden.
type EndpointFunc func(metaData MetaData) string

// StaticEndpoint liefert immer unabhängig von den Metadaten den übergebenen
// Endpunkt.
func StaticEndpoint(endpoint string) EndpointFunc {
	return func(_ MetaData) string {
		return endpoint
	}
}

// RestGateway leitet Daten an einen REST-Endpunkt weiter. Das Gateway kann
// u.a. mit folgenden Features konfiguriert werden:
// 		- HTTP/HTTPS
//		- Auth: JWT, Basic, etc.
//		- FIFO, LIFO beim Versenden
//		- persistente Zwischenspeicherung der zu sendenden Daten
//		- Senden beliebiger Daten, z.B. JSON, Text, Bilder, etc.
//		- Strategie zum erneuten Senden fehlgeschlagener Übertragungen
type RestGateway struct {
	endpointFunc    EndpointFunc
	client          Client
	strategy        TransmissionStrategy
	requestExtender RequestExtender
	backOffDuration time.Duration
	logger          Logger
	shutdownChan    chan bool
	muxSendItems    sync.Mutex
	sendingItems    bool
}

// AddItem fügt zu sendende Daten dem Gateway hinzu. Leitet die Daten an die
// verwendete TransmissionStrategy weiter.
func (g *RestGateway) AddItem(metaData MetaData, r io.Reader) error {
	err := g.strategy.AddItem(metaData, r)
	if err != nil {
		return err
	}
	go g.sendItems()
	return nil
}

// sendItems sendet die zwischengespeicherten Elemente, die durch AddItem()
// dem Gateway hinzugefügt wurden. Welche Elemente gesendet werden, bestimmt
// die TransmissionStrategy. Die einzelnen Elemente werden seriell abgearbeitet.
func (g *RestGateway) sendItems() {
	defer func() {
		g.muxSendItems.Lock()
		g.sendingItems = false
		g.muxSendItems.Unlock()
	}()
	g.muxSendItems.Lock()
	if g.sendingItems {
		g.muxSendItems.Unlock()
		return
	}
	g.sendingItems = true
	g.muxSendItems.Unlock()
	item := g.getNextItem()
	for item != nil {
		endpoint := g.endpointFunc(item.MetaData)
		result := g.sendItem(item)
		if result.Error != nil {
			g.logger.Error(Fields{
				"endpoint": endpoint,
				"uuid":     item.MetaData.Uuid,
				"error":    result.Error,
			}, "Fehler beim Senden der Daten.")
		} else {
			g.logger.Debug(Fields{
				"endpoint":   endpoint,
				"uuid":       item.MetaData.Uuid,
				"statusCode": result.Response.StatusCode,
			}, "Übertragung beendet.")
		}
		if !g.strategy.OnTransmissionFinished(result) {
			result.DrainAndCloseBody()
			return
		}
		result.DrainAndCloseBody()
		item = g.getNextItem()
	}
}

// getNextItem liefert das als nächstes zu sendende Element. Wenn kein Element
// mehr zum Senden existiert oder zur Zeit kein Element gesendet werden soll,
// dann wird nil zurückgegeben.
func (g *RestGateway) getNextItem() *Item {
	item, err := g.strategy.NextItem()
	if err != nil {
		if err != ErrorNoItemsInStore {
			g.logger.Error(Fields{"error": err}, "Fehler beim Bestimmen des nächsten zu sendenden Elements.")
		}
		return nil
	}
	return item
}

// sendItem sendet das übergebene Element an den REST-Endpunkt.
func (g *RestGateway) sendItem(item *Item) *TransmissionResult {
	result := TransmissionResult{
		MetaData: item.MetaData,
	}
	req, err := http.NewRequest(http.MethodPost, g.endpointFunc(item.MetaData), item.reader)
	if err != nil {
		result.Error = err
		return &result
	}
	req.Header.Set("Content-Type", item.MetaData.ContentType)
	if err := g.requestExtender.ExtendRequest(req); err != nil {
		result.Error = err
		return &result
	}
	result.Response, result.Error = g.client.Do(req)
	return &result
}

// Start startet das RestGateway und damit das kontinuierliche Senden der hinzugefügten Daten.
// Diese Methode blockt und wird erst nach Verwendung von Shutdown() beendet.
func (g *RestGateway) Start() error {
	g.shutdownChan = make(chan bool)

Loop:
	for {
		select {
		case <-time.After(g.backOffDuration):
			g.sendItems()
		case <-g.shutdownChan:
			break Loop
		}
	}
	return nil
}

// Shutdown beendet das RestGateway.
func (g *RestGateway) Shutdown() error {
	if g.shutdownChan != nil {
		close(g.shutdownChan)
	}
	return nil
}

// applyDefaultsAndCheck ergänzt die Optionen mit den Standardwerten und
// überprüft anschließend die Optionen auf Vollständigkeit.
func applyDefaultsAndCheck(opts *Options) error {
	if opts.Endpoint != "" {
		opts.EndpointFunc = StaticEndpoint(opts.Endpoint)
	}
	if opts.EndpointFunc == nil {
		return ErrorEndpointFuncMissing
	}
	if opts.Strategy == nil {
		strategy, err := NewInMemoryFifoStrategy()
		if err != nil {
			return err
		}
		opts.Strategy = strategy
	}
	if opts.Client == nil {
		opts.Client = http.DefaultClient
	}
	if opts.Logger == nil {
		opts.Logger = NewNoopLogger()
	}
	if opts.RequestExtender == nil {
		opts.RequestExtender = NewNoopRequestExtender()
	}
	if opts.BackOffDuration.Nanoseconds() == 0 {
		opts.BackOffDuration = DefaultBackOffDuration
	}
	return nil
}
