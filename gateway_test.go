package restgateway_test

import (
	"bitbucket.org/panitz/rest-gateway"
	"github.com/h2non/gock"
	"github.com/stretchr/testify/require"
	"io"
	"net/http"
	"os"
	"path"
	"testing"
	"time"
)

var (
	testStoreDir = getPath("temp", "test-store-dir")
)

func TestRestGateway_SuccessfulTransmission(t *testing.T) {
	_ = os.RemoveAll(testStoreDir)
	defer func() { _ = os.RemoveAll(testStoreDir) }()
	defer gock.Off()
	requests := 0
	expectedRequests := 1
	expectedRequestsReceived := make(chan bool)
	gock.Observe(func(request *http.Request, mock gock.Mock) {
		requests++
		if requests >= expectedRequests {
			expectedRequestsReceived <- true
		}
	})

	gock.New("http://api.example.com").
		Post("/api/status").
		MatchHeader("Content-Type", "application/json").
		File("testdata/json1.json").
		Times(1).
		Reply(http.StatusNoContent)

	store, err := restgateway.NewFileStore(testStoreDir)
	require.NoError(t, err)
	strategy, err := restgateway.NewFifoStrategy(store, "")
	require.NoError(t, err)
	opts := restgateway.Options{
		Endpoint:        "http://api.example.com/api/status",
		Strategy:        strategy,
		Client:          http.DefaultClient,
		BackOffDuration: time.Millisecond * 100,
	}
	gateway, err := restgateway.New(opts)
	require.NoError(t, err)
	go func() { _ = gateway.Start() }()
	defer func() { _ = gateway.Shutdown() }()

	file, err := os.Open("testdata/json1.json")
	require.NoError(t, err)
	require.NoError(t, gateway.AddItem(restgateway.NewMetaData("application/json"), file))

loop:
	for {
		select {
		case <-expectedRequestsReceived:
			break loop
		case <-time.After(time.Second):
			break loop
		}
	}

	require.True(t, gock.IsDone(), "Not all expected requests were fulfilled.")
	require.Equal(t, expectedRequests, requests)

	time.Sleep(time.Millisecond * 50)
	isEmpty, err := isDirEmpty(path.Join(testStoreDir, ".meta"))
	require.NoError(t, err)
	require.True(t, isEmpty, "Store directory should be empty")
}

func TestRestGateway_SuccessAfterFailed(t *testing.T) {
	_ = os.RemoveAll(testStoreDir)
	defer func() { _ = os.RemoveAll(testStoreDir) }()
	defer gock.Off()
	requests := 0
	expectedRequests := 2
	expectedRequestsReceived := make(chan bool)
	gock.Observe(func(request *http.Request, mock gock.Mock) {
		requests++
		if requests >= expectedRequests {
			expectedRequestsReceived <- true
		}
	})

	gock.New("http://www.example.com").
		Post("/api/images").
		MatchHeader("Content-Type", "text/plain").
		File("testdata/textfile.txt").
		Times(1).
		Reply(http.StatusForbidden)
	gock.New("http://www.example.com").
		Post("/api/images").
		MatchHeader("Content-Type", "text/plain").
		File("testdata/textfile.txt").
		Times(1).
		Reply(http.StatusCreated)

	store, err := restgateway.NewFileStore(testStoreDir)
	require.NoError(t, err)
	strategy, err := restgateway.NewFifoStrategy(store, "")
	require.NoError(t, err)
	opts := restgateway.Options{
		Endpoint:        "http://www.example.com/api/images",
		Strategy:        strategy,
		Client:          http.DefaultClient,
		BackOffDuration: time.Millisecond * 100,
	}
	gateway, err := restgateway.New(opts)
	require.NoError(t, err)
	go func() { _ = gateway.Start() }()
	defer func() { _ = gateway.Shutdown() }()

	file, err := os.Open("testdata/textfile.txt")
	require.NoError(t, err)
	require.NoError(t, gateway.AddItem(restgateway.NewMetaData("text/plain"), file))

loop:
	for {
		select {
		case <-expectedRequestsReceived:
			break loop
		case <-time.After(time.Second):
			break loop
		}
	}

	require.True(t, gock.IsDone(), "Not all expected requests were fulfilled.")
	require.Equal(t, expectedRequests, requests)

	time.Sleep(time.Second * 1)
	isEmpty, err := isDirEmpty(path.Join(testStoreDir, ".meta"))
	require.NoError(t, err)
	require.True(t, isEmpty, "Store directory should be empty")
}

func TestRestGateway_Failing(t *testing.T) {
	_ = os.RemoveAll(testStoreDir)
	defer func() { _ = os.RemoveAll(testStoreDir) }()
	defer gock.Off()

	gock.New("http://www.example.com").
		Post("/api/images").
		MatchHeader("Content-Type", "text/plain").
		File("testdata/textfile.txt").
		Reply(http.StatusForbidden)

	store, err := restgateway.NewFileStore(testStoreDir)
	require.NoError(t, err)
	strategy, err := restgateway.NewFifoStrategy(store, "")
	require.NoError(t, err)
	opts := restgateway.Options{
		Endpoint:        "http://www.example.com/api/images",
		Strategy:        strategy,
		Client:          http.DefaultClient,
		BackOffDuration: time.Millisecond * 100,
	}
	gateway, err := restgateway.New(opts)
	require.NoError(t, err)
	go func() { _ = gateway.Start() }()
	defer func() { _ = gateway.Shutdown() }()

	file, err := os.Open("testdata/textfile.txt")
	require.NoError(t, err)
	require.NoError(t, gateway.AddItem(restgateway.NewMetaData("text/plain"), file))

loop:
	for {
		select {
		case <-time.After(time.Second):
			break loop
		}
	}

	require.True(t, gock.IsDone(), "Not all expected requests were fulfilled.")
	itemsInStore, err := store.ListAllItems()
	require.NoError(t, err)
	require.NotEqual(t, 0, len(itemsInStore))
}

func isDirEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	// read in ONLY one file
	_, err = f.Readdir(1)

	// and if the file is EOF... well, the dir is empty.
	if err == io.EOF {
		return true, nil
	}
	return false, err
}
