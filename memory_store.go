package restgateway

import (
	"bytes"
	"container/list"
	"io"
	"io/ioutil"
	"sync"
)

// NewMemoryStore erstellt einen neuen MemoryStore.
func NewMemoryStore() *MemoryStore {
	return &MemoryStore{}
}

// NewMemoryStoreWithCapacity erstellt einen neuen MemoryStore, in dem
// maximal maxItems gespeichert werden. Ist die Kapazität überschritten, dann
// wird das älteste Element gelöscht.
func NewMemoryStoreWithCapacity(maxItems int) *MemoryStore {
	return &MemoryStore{maxItems: maxItems}
}

// MemoryStore ist eine Store-Implementation, der den Hauptsoeicher zum Speichern
// der Daten verwendet. Die Daten gehen bei einem Neustart des Programms verloren!
// Der MemoryStore eignet sich daher nicht für wichtige Daten!
type MemoryStore struct {
	maxItems int
	items    list.List
	mux      sync.Mutex
}

type memoryItem struct {
	metaData MetaData
	bytes    []byte
}

// AddItem fügt die Daten dem Store hinzu. Ist das Maximum an Elementen erreicht,
// dann wird das älteste Element entfernt.
func (s *MemoryStore) AddItem(metaData MetaData, r io.Reader) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	item := memoryItem{
		metaData: metaData,
		bytes:    b,
	}
	if s.maxItems > 0 && s.items.Len() >= s.maxItems {
		s.items.Remove(s.items.Front())
	}
	s.items.PushBack(item)
	return nil
}

// GetItem liefert den Inhalt eines Elementes des Stores. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *MemoryStore) GetItem(uuid string) (io.ReadCloser, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	for e := s.items.Front(); e != nil; e = e.Next() {
		item := e.Value.(memoryItem)
		if item.metaData.Uuid == uuid {
			return NewClosableByteReader(item.bytes), nil
		}
	}
	return nil, ErrorItemNotFound
}

// GetItemMetaData liefert die Metadaten eines Elementes im Store. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *MemoryStore) GetItemMetaData(uuid string) (MetaData, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	for e := s.items.Front(); e != nil; e = e.Next() {
		item := e.Value.(memoryItem)
		if item.metaData.Uuid == uuid {
			return item.metaData, nil
		}
	}
	return MetaData{}, ErrorItemNotFound
}

// RemoveItem entfernt Daten und Metadaten eines Elementes aus dem Store. Ist das gesuchte Element
// nicht im Store vorhanden, dann wird ErrorItemNotFound zurückgegeben.
func (s *MemoryStore) RemoveItem(uuid string) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	for s.items.Len() > 0 {
		e := s.items.Front() // First element
		if e.Value.(memoryItem).metaData.Uuid == uuid {
			s.items.Remove(e) // Dequeue
			return nil
		}
	}
	return ErrorItemNotFound
}

// ListAllItems liefert die Metadaten der Elemente, die sich im Store befinden. Es wird eine leere
// Liste zurückgeben, wenn keine Elemente im Store vorhanden sind.
func (s *MemoryStore) ListAllItems() ([]MetaData, error) {
	s.mux.Lock()
	defer s.mux.Unlock()
	var datas []MetaData
	for e := s.items.Front(); e != nil; e = e.Next() {
		datas = append(datas, e.Value.(memoryItem).metaData)
	}
	return datas, nil
}

// NewClosableByteReader erstellt einen neuen closableByteReader.
func NewClosableByteReader(b []byte) io.ReadCloser {
	return &closableByteReader{reader: bytes.NewReader(b)}
}

// closableByteReader ist ein bytes.Reader, das io.Closer implementiert.
type closableByteReader struct {
	reader io.Reader
}

func (*closableByteReader) Close() error {
	return nil
}

func (r *closableByteReader) Read(p []byte) (n int, err error) {
	return r.reader.Read(p)
}
