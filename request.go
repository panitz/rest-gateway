package restgateway

import (
	"encoding/base64"
	"fmt"
	"net/http"
)

// NewNoopRequestExtender liefert einen neuen NoopRequestExtender.
func NewNoopRequestExtender() RequestExtender {
	return &NoopRequestExtender{}
}

// NoopRequestExtender ist ein RequestExtender, der eine leere ExtendRequest-Methode
// besitzt.
type NoopRequestExtender struct{}

// ExtendRequest ist leer und gibt immer nil zurück.
func (NoopRequestExtender) ExtendRequest(_ *http.Request) error {
	return nil
}

// NewTokenAuth erstellt einen neuen TokenAuthExtender mit dem übergebenen
// TokenProvider.
func NewTokenAuth(provider TokenProvider) RequestExtender {
	return &TokenAuthExtender{tokenProvider: provider}
}

// TokenAuthExtender ist ein RequestExtender, der den Request um einen
// Authorization-Header mit Aufbau "Bearer token" erweitert. Das Token wird
// dabei durch den TokenProvider geliefert.
type TokenAuthExtender struct {
	tokenProvider TokenProvider
}

// TokenProvider bietet Methoden zum Ermitteln eines Token zur Verfügung.
type TokenProvider interface {
	// Token liefert das zu verwendede Token.
	Token() (string, error)
}

// ExtendRequest fügt dem Request einen Authorization-Header mit dem Token
// aus dem TokenProvider hinzu.
func (e *TokenAuthExtender) ExtendRequest(req *http.Request) error {
	token, err := e.tokenProvider.Token()
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return nil
}

// NewBasicAuthExtender erstellt einen BasicAuthExtender für die übergebene
// Benutzer/Passwort Kombination.
func NewBasicAuthExtender(username string, password string) RequestExtender {
	return &BasicAuthExtender{username: username, password: password}
}

// BasicAuthExtender fügt dem Request Basic Authentication Informationen hinzu.
type BasicAuthExtender struct {
	username string
	password string
}

// ExtendRequest fügt dem Request einen Authorization-Header mit Basic Authentication
// hinzu.
func (e *BasicAuthExtender) ExtendRequest(req *http.Request) error {
	encoded := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", e.username, e.password)))
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", encoded))
	return nil
}
